//
//  SideMenuVC.swift
//  Practical_test
//
//  Created by Hitendra on 19/12/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
protocol SideMenuDelegate : AnyObject {
    func didcancel()
}

class SideMenuVC: UIViewController {

    @IBOutlet weak var gestureView: UIView!
    @IBOutlet weak var leftConstraint : NSLayoutConstraint!
    weak var delegate : SideMenuDelegate?
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var lblusername: UILabel!
    @IBOutlet weak var lblemail: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileData()
    }
    func profileData(){
        let registerUser = DBManager.sharedInstance.getUser()
        let user = registerUser.firstObject as? User
        self.lblemail.text = user?.email
        self.lblusername.text = user?.username
        if user?.profileimage != nil {
            URLImagedownload.sharedInstance.downlodImage(user!.profileimage!) { (data, error) in
                if error == nil {
                    self.profileImage.image = UIImage(data: data! as Data)
                }
            }
        }
        
    }
    
    func setUpView(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.gestureView!.addGestureRecognizer(tap)
        self.leftConstraint.constant = -230
        self.view.backgroundColor = UIColor.darkGray.withAlphaComponent(0.8)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
            self.leftConstraint.constant = 0
            UIView.animate(withDuration: 0.4,animations: {
                self.view.layoutIfNeeded()
                self.view.backgroundColor = UIColor.darkGray.withAlphaComponent(0.7)
            })
        }
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.leftConstraint.constant = -230
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }, completion: { (bool) in
            self.delegate!.didcancel()
        })
    }
    
    @IBAction func menuButtonAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        if(sender.tag == 1){ // Change Password
          loginVC.isFromChagePassword = true
        }
        else if(sender.tag == 2){ // Logout
            let registerUser = DBManager.sharedInstance.getUser()
            DBManager.sharedInstance.deleteObject(registerUser.firstObject as! User)
            GIDSignIn.sharedInstance()?.signOut()
            
            let loginManager = LoginManager()
            loginManager.logOut() // this is an instance function
            
            LocationManager.sharedManger.stopUpdateLocation()
        }
        self.navigationController!.viewControllers = [loginVC]
    }

}
