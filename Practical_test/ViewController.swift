//
//  ViewController.swift
//  Practical_test
//
//  Created by Hitendra on 19/12/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let registerUser = DBManager.sharedInstance.getUser()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if registerUser.count == 0{
            let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(loginVC, animated: false)
        }
        else{
            let dashBroad = storyboard.instantiateViewController(withIdentifier: "DashboradVC") as! DashboradVC
            self.navigationController?.pushViewController(dashBroad, animated: false)
        }
    }
}

