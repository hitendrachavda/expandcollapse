//
//  HomeVC.swift
//  Practical_test
//
//  Created by Hitendra on 19/12/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class CustomCell : UITableViewCell{
    @IBOutlet weak var heightConstarint : NSLayoutConstraint?
    @IBOutlet weak var titleName : UILabel!
    @IBOutlet weak var viewTime : UIView!
    @IBOutlet weak var stackView : UIStackView!

}

// Category Model
struct Category {
    var title : String
    var subCategory = [SubCategory]()
}
// SubCategory Model
struct SubCategory {
    var title : String
}

class HomeVC: UIViewController {

    @IBOutlet weak var tblView : UITableView!
      
       var isExpanded : Bool = false
       var indexPath : IndexPath?
    
    var expandedSections = NSMutableIndexSet()
    
    var categoryArray = [
        Category(title: "Category 1", subCategory: [
            SubCategory(title: "SubCategory 1"),
            SubCategory(title: "SubCategory 2"),
            SubCategory(title: "SubCategory 3"),
        ]),
        Category(title: "Category 2", subCategory: [
            SubCategory(title: "SubCategory 1"),
            SubCategory(title: "SubCategory 2"),
            SubCategory(title: "SubCategory 3"),
            SubCategory(title: "SubCategory 4"),
        ]),
        Category(title: "Category 3", subCategory: [
            SubCategory(title: "SubCategory 1"),
            SubCategory(title: "SubCategory 2"),
        ]),
        Category(title: "Category 4", subCategory: [
            SubCategory(title: "SubCategory 1"),
            SubCategory(title: "SubCategory 2"),
            SubCategory(title: "SubCategory 3"),
            SubCategory(title: "SubCategory 4"),
            SubCategory(title: "SubCategory 5"),
        ])
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    func tableView(_ tableView : UITableView, _ canCollapseCell : NSInteger) -> Bool{
        if canCollapseCell > 0 { return true}
        return false
    }

}
extension HomeVC: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categoryArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.expandedSections.contains(section)){
            let category = self.categoryArray[section]
            return category.subCategory.count + 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
            
            if indexPath.row == 0 {
                let headercell = tableView.dequeueReusableCell(withIdentifier: "CustomCellHeader", for: indexPath) as? CustomCell
                cell = headercell!
                let category = self.categoryArray[indexPath.section]
                headercell!.titleName.text = category.title
            }
            else{
                let headercell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as? CustomCell
                let category = self.categoryArray[indexPath.section]
                let subCate = category.subCategory[indexPath.row-1]
                headercell!.titleName.text = subCate.title
                cell = headercell!
            }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.indexPath != nil && self.indexPath != indexPath){
            self.tblView.beginUpdates()
            let currentlyExpanded1 = expandedSections.contains(self.indexPath!.section)
            var rows1 : Int = 0
            let tmpArray1 = NSMutableArray()
            if currentlyExpanded1 {
                let category = self.categoryArray[self.indexPath!.section]
                rows1 = category.subCategory.count
                self.expandedSections.remove(self.indexPath!.section)
            }
            for i in (1..<rows1+1){
                let indexPath = IndexPath(row: i, section: self.indexPath!.section)
                tmpArray1.add(indexPath)
            }
            if (currentlyExpanded1){
                self.tblView.deleteRows(at: tmpArray1 as! [IndexPath], with: .none)
            }
            self.tblView.endUpdates()
        }
        
        
        if indexPath.row == 0{
                self.tblView.beginUpdates()
                self.tblView.deselectRow(at: indexPath, animated: true)
                
                let section =  indexPath.section
                let currentlyExpanded = self.expandedSections.contains(section)
                
                var rows : Int = 0
                
                let tmpArray = NSMutableArray()
                self.indexPath = indexPath
                if currentlyExpanded {
                    let category = self.categoryArray[indexPath.section]
                    rows = category.subCategory.count
                    self.expandedSections.remove(section)
                }
                else{
                    self.expandedSections.add(section)
                    let category = self.categoryArray[indexPath.section]
                    rows = category.subCategory.count
                }
                
                for i in (1..<rows+1){
                    let indexPath = IndexPath(row: i, section: section)
                    tmpArray.add(indexPath)
                }
                
                if (currentlyExpanded){
                    self.tblView.deleteRows(at: tmpArray as! [IndexPath], with: .top)
                }
                else{
                    self.tblView.insertRows(at: tmpArray as! [IndexPath], with: .top)
                }
                self.tblView.endUpdates()
            }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let dateWise = self.shiftInOutArray[section]
//        let view = UIView()
//        let vc = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
//        vc.backgroundColor = .lightGray
//        // Header Title Label
//        let lable = UILabel()
//        lable.font = lable.font.withSize(20.0)
//        lable.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        lable.text = dateWise.title
//        lable.textColor = .black
//
//        let stackView  = UIStackView()
//        stackView.axis = .horizontal
//        stackView.distribution = .fill
//        stackView.alignment = .fill
//        stackView.spacing = 10
//
//        stackView.addArrangedSubview(lable)
//        stackView.translatesAutoresizingMaskIntoConstraints = false;
//        vc.addSubview(stackView)
//        stackView.topAnchor.constraint(equalTo: vc.topAnchor, constant: 0).isActive = true
//        stackView.bottomAnchor.constraint(equalTo: vc.bottomAnchor, constant: 0).isActive = true
//        stackView.widthAnchor.constraint(equalTo: vc.widthAnchor, constant: 0).isActive = true
//        stackView.heightAnchor.constraint(equalTo: vc.heightAnchor, constant: 0).isActive = true
//        view.addSubview(vc)
//        return view
        
        return UIView()
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}
