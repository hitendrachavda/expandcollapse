//
//  MapVC.swift
//  Practical_test
//
//  Created by Hitendra on 19/12/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController {

     @IBOutlet weak var mMapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mMapView.delegate = self
    }
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    func startUpdateLocation(){
        LocationManager.sharedManger.startLocationManager(self)
    }
}

extension MapVC : LocationManagerDelegate{
    
    func didGetUserLocationInfo(location: CurrentLocation?) {
        guard location != nil else {
            return
        }

        let coordinate = CLLocationCoordinate2D(latitude: location!.latitude, longitude: location!.longitude)
        let userLocation = UserAnnotation(coordinate: coordinate)
        userLocation.placeName = location!.placeName
        userLocation.locality = location!.locality
        userLocation.sublocality = location!.sublocality
        userLocation.administrativeArea = location!.administrativeArea
        userLocation.postalCode = location!.postalCode
        userLocation.country = location!.country
    
        let allAnnotations = mMapView.annotations
        if(allAnnotations.count > 0){
            mMapView.removeAnnotations(allAnnotations)
        }
        mMapView.addAnnotation(userLocation)
        
        let region = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
        mMapView.setRegion(region, animated: true)
    }

}
extension MapVC : MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation
        {
            return nil
        }
        var annotationView = self.mMapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if annotationView == nil{
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = false
        }else{
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: "shape1")
        return annotationView
    }
    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView)
    {
        // 1
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        // 2
        let userannotation = view.annotation as! UserAnnotation
        let views = Bundle.main.loadNibNamed("CustomCalloutView", owner: nil, options: nil)
        let calloutView = views?[0] as! CustomCalloutView
    
        let address = "\(userannotation.placeName ?? ""), \(userannotation.locality ?? ""), \(userannotation.sublocality ?? ""), \(userannotation.administrativeArea ?? "")"
        calloutView.currentaddress.text = address
        calloutView.postalCode.text = userannotation.postalCode
        calloutView.country.text = userannotation.country
        calloutView.currentaddress.sizeToFit()
        let height = calloutView.currentaddress.frame.size.height
        calloutView.frame.size = CGSize(width: calloutView.frame.size.width, height: calloutView.frame.size.height + height - 21)
        view.addSubview(calloutView)
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
        view.addSubview(calloutView)
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.isKind(of: AnnotationView.self)
        {
            for subview in view.subviews
            {
                subview.removeFromSuperview()
            }
        }
    }
    
}
