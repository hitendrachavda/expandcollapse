//
//  DBManager.swift
//  Practical_test
//
//  Created by Hitendra on 19/12/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DBManager : NSObject {
    
    static let sharedInstance = DBManager()
    
    func mainMoc() -> NSManagedObjectContext {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { fatalError("Could not convert delegate to AppDelegate") }
        return appDelegate.persistentContainer.viewContext
    }
    
    func getUser() -> NSArray {
           let users = NSMutableArray()
           let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
           do {
               let results = try self.mainMoc().fetch(fetchRequest)
               for info in results {
                   users.add(info)
               }
           } catch  {
               return users
           }
           return users
       }
    
    func saveLoginUser(_ userDict : [String : Any]) {
        let user = NSEntityDescription.insertNewObject(forEntityName: "User", into: self.mainMoc()) as? User
        user?.email = (userDict["email"] as? String)
        user?.password = (userDict["password"] as? String)
        if let loginfromSocial = userDict["loginFromSocial"] as? Bool {
             user?.loginFromSocial = loginfromSocial
        }
        user?.username = (userDict["username"] as? String)
        user?.profileimage = (userDict["profileimage"] as? String)
        self.saveContext()
    }
    func changePassword(_ password : String) {
        let registerUser = self.getUser()
        let user = registerUser.firstObject as? User
        user!.password = password
        self.saveContext()
    }

    func saveContext(){
        do {
            try self.mainMoc().save()
        } catch  {
            print("data is not saved")
        }
    }
    
    func deleteObject(_ object : NSManagedObject){
        self.mainMoc().delete(object)
        self.saveContext()
    }
    
    
}
