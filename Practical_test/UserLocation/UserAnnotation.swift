//
//  UserAnnotation.swift
//  Practical_test
//
//  Created by Hitendra on 19/12/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import MapKit

class UserAnnotation: NSObject, MKAnnotation {
    
    var placeName : String?
    var locality : String?
    var sublocality : String?
    var administrativeArea : String?
    var postalCode : String?
    var country : String?
    
    var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}
