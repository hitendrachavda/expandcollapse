//
//  LoginVC.swift
//  Practical_test
//
//  Created by Hitendra on 19/12/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit


class LoginVC: UIViewController {

    @IBOutlet weak var loginButton: UIButton?
    
    @IBOutlet weak var headerView: UIView?
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var facebookBtnView: UIView!
    @IBOutlet weak var socialNetworkView: UIStackView!
    
    var isFromChagePassword : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.isFromChagePassword){
            self.loginButton?.setTitle("Change Password", for: .normal)
            let registerUser = DBManager.sharedInstance.getUser()
            let user = registerUser.firstObject as? User
            self.txtemail?.text! =  user!.email!
            self.socialNetworkView.isHidden = true
        }
    }
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    let userInfo = result as? [String : Any]
                    self.storeInfoToDB(userInfo!)
                }
            })
        }
    }
    
    func storeInfoToDB(_ userInfo : [String : Any]){
        let profilePic = (userInfo["picture"] as! [String : Any])
        let data = (profilePic["data"] as! [String : Any])
        let loginDict : [String : Any] = [
            "email" : (userInfo["email"] as! String),
            "username" : (userInfo["name"] as! String),
            "profileimage" : (data["url"] as! String),
            "loginFromSocial" : true
        ]
        DBManager.sharedInstance.saveLoginUser(loginDict)
        self.pushToDashBoard()
    }
    
    @IBAction func passwordShow(_ sender : UIButton){
        self.txtPassword.isSecureTextEntry = !self.txtPassword.isSecureTextEntry
    }
    
    // Facebook Login
    @IBAction func loginWithFB(_ sender : UIButton){
        
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) -> Void in
           if (error == nil){
            let fbloginresult : LoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }

    // Google Login
    @IBAction func loginWithGoogle(_ sender : UIButton){
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func submit(_ sender : UIButton){
        self.view.endEditing(true)
        var message : String = ""
        if(self.isFromChagePassword){
            if txtPassword?.text!.trim().count == 0 {
                message = "Enter password"
            }
            if (message.count > 0){
                AlertController.shared.showAlert(fromVC: self, title: "Covid19", message: message, buttonTitles: ["Ok"],handlars:nil)
                return
            }
            DBManager.sharedInstance.changePassword(self.txtPassword.text!)
            self.pushToDashBoard()
        }
        else{
            if txtemail?.text!.trim().count == 0 {
                message = "Enter email address"
            }
            else if txtPassword?.text!.trim().count == 0 {
                message = "Enter password"
            }
            else  if txtemail?.text! == "iflair@gmail.com" || txtemail?.text! == "iflair@mailinator.com" {
                let loginDict : [String : Any] = [
                    "email" : self.txtemail.text! as String,
                    "password" : self.txtPassword.text! as String,
                ]
                DBManager.sharedInstance.saveLoginUser(loginDict)
                self.pushToDashBoard()
            }
            else{
                message = "Enter valid email address"
            }
        }
        if (message.count > 0){
            AlertController.shared.showAlert(fromVC: self, title: "Covid19", message: message, buttonTitles: ["Ok"],handlars:nil)
            return
        }
    
    }
    
    func pushToDashBoard(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let dashBraod = storyboard.instantiateViewController(withIdentifier: "DashboradVC") as! DashboradVC
        self.navigationController?.pushViewController(dashBraod, animated: false)
    }
}

extension LoginVC : GIDSignInDelegate {
    
    func signInWillDispatch(_ signIn: GIDSignIn!, error: Error!) {
    }
    
    func signIn(_ signIn: GIDSignIn!,
        presentViewController viewController: UIViewController!) {
      self.present(viewController, animated: true, completion: nil)
    }
    func signIn(_ signIn: GIDSignIn!,
        dismissViewController viewController: UIViewController!) {
      self.dismiss(animated: true, completion: nil)
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        let loginDict : [String : Any] = [
            "email" : user.profile.email as String,
            "username" : user.profile.name as String,
            "loginFromSocial" : true
        ]
        DBManager.sharedInstance.saveLoginUser(loginDict)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let dashBraod = storyboard.instantiateViewController(withIdentifier: "DashboradVC") as! DashboradVC
        self.navigationController?.pushViewController(dashBraod, animated: false)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
    }
}

extension LoginVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}
