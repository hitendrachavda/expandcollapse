//
//  LocationManager.swift
//  Practical_test
//
//  Created by Hitendra on 19/12/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import Foundation
import CoreLocation
import CoreMotion

// Curent Location Model
struct CurrentLocation {
    var placeName : String?
    var locality : String?
    var sublocality : String?
    var administrativeArea : String?
    var postalCode : String?
    var country : String?
    
    var latitude : CLLocationDegrees
    var longitude : CLLocationDegrees
    
}
protocol LocationManagerDelegate : AnyObject {
    func didGetUserLocationInfo(location : CurrentLocation?)
}

class LocationManager: NSObject {
    
    static let sharedManger = LocationManager()
    var locationManager:CLLocationManager!
    weak var delegate : LocationManagerDelegate?
    
    var locationManagerStarted : Bool = false
    
    func startLocationManager(_ delegate : LocationManagerDelegate) {
        print("locationManager")
        self.locationManagerStarted = true
        self.delegate = delegate
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.distanceFilter = 300 // distance changes you want to be informed about (in meters)
        locationManager.desiredAccuracy = 10 // biggest approximation you tolerate (in meters)
        locationManager.activityType = .automotiveNavigation // .automotiveNavigation will stop the updates when the device is not moving
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    func stopUpdateLocation(){
        if(self.locationManager != nil){
            self.locationManager.stopUpdatingLocation()
            self.locationManager = nil
            self.locationManagerStarted = false
        }
    }
    
    func startUpdateLocation(){
        locationManager.startUpdatingHeading()
        locationManager.startUpdatingLocation()
    }
    func getUsersClosestLocation(mLattitude: CLLocationDegrees, mLongitude: CLLocationDegrees) {
        let geocoder = CLGeocoder()
        let location = CLLocation(latitude: mLattitude, longitude: mLongitude)
        geocoder.reverseGeocodeLocation(location) { (placemarksArray, error) in
            if (error) == nil {
                if placemarksArray!.count > 0 {
                    let placemark = placemarksArray?[0]
                    let currentAddress = CurrentLocation(placeName: placemark?.name ?? "", locality: placemark?.locality ?? "", sublocality: placemark?.subLocality ?? "", administrativeArea: placemark?.administrativeArea ?? "", postalCode: placemark?.postalCode ?? "", country: placemark?.country ?? "",latitude:mLattitude,longitude: mLongitude )
                    self.delegate!.didGetUserLocationInfo(location: currentAddress)
                }
            }
            else{
                print("Error getting locatin info")
            }
        }
    }
    
}
extension LocationManager: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard locations.count > 0 else {
            return
        }
        let mUserLocation:CLLocation = locations[0] as CLLocation
        self.getUsersClosestLocation(mLattitude: mUserLocation.coordinate.latitude, mLongitude: mUserLocation.coordinate.longitude)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("Error - locationManager: \(error.localizedDescription)")
    }
}

