//
//  TabBarController.swift
//  Practical_test
//
//  Created by Hitendra on 19/12/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class DashboradVC: UIViewController {

    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnMap: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var homeViewContainer: UIView!
    @IBOutlet weak var mapviewContainer: UIView!
    @IBOutlet weak var sideMenuContainer: UIView!
    
    var mapVC : MapVC?
    var sideMenuVC : SideMenuVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitle.text = "Home"
        self.btnHome.tintColor = .white
        self.btnHome.isHidden = false
        self.mapviewContainer!.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnTabClicked(_ sender: UIButton) {
        self.btnHome.tintColor = UIColor(named: "BgColor")
        self.btnMap.tintColor = UIColor(named: "BgColor")
        self.homeViewContainer!.isHidden = true
        self.mapviewContainer!.isHidden = true
        sender.tintColor = .white
        if(sender.tag == 1){
            self.lblTitle.text = "Home"
            self.homeViewContainer!.isHidden = false
        }
        else if(sender.tag == 2){
            self.lblTitle.text = "Map"
            self.mapviewContainer!.isHidden = false
            if(LocationManager.sharedManger.locationManagerStarted == false){
                self.mapVC!.startUpdateLocation()
            }
        }
    }
    
    @IBAction func btnMenuClicked(_ sender: UIButton) {
        if(self.sideMenuContainer.isHidden){
            self.sideMenuVC!.setUpView()
            self.sideMenuContainer.isHidden = false
        }
        else{
            self.sideMenuContainer.isHidden = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MapVC" {
            self.mapVC = (segue.destination as! MapVC)
        }
        if segue.identifier == "SideMenuVC" {
            self.sideMenuVC = (segue.destination as! SideMenuVC)
            self.sideMenuVC!.delegate = self
        }
    }

}

extension DashboradVC : SideMenuDelegate {
    func didcancel() {
        self.sideMenuContainer.isHidden = true
    }

}
