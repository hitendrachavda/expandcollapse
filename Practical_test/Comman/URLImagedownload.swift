//
//  WebServiceManager.swift
//  MVVM
//
//  Created by Hitendra on 04/06/20.
//

import UIKit

class URLImagedownload: NSObject {
    
    static let sharedInstance = URLImagedownload()
    
    func downlodImage(_ imgUrl : String , complition : @escaping(NSData?,Error?) -> ()){
        var responseData : Data? = nil
        URLSession.shared.dataTask(with: URL(string: imgUrl)!) { (data, response, error) in
            if error == nil{
                DispatchQueue.main.async {
                    responseData = data
                    complition(responseData as NSData?,nil)
                }
            }
        }.resume()
    }
}
